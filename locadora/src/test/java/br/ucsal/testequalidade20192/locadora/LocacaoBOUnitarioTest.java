package br.ucsal.testequalidade20192.locadora;

import static org.powermock.api.mockito.PowerMockito.mockStatic;
import static org.powermock.api.mockito.PowerMockito.verifyNoMoreInteractions;
import static org.powermock.api.mockito.PowerMockito.verifyStatic;
import static org.powermock.api.mockito.PowerMockito.when;
import static org.powermock.api.mockito.PowerMockito.whenNew;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import br.ucsal.testequalidade20192.locadora.business.LocacaoBO;
import br.ucsal.testequalidade20192.locadora.dominio.Cliente;
import br.ucsal.testequalidade20192.locadora.dominio.Locacao;
import br.ucsal.testequalidade20192.locadora.dominio.Modelo;
import br.ucsal.testequalidade20192.locadora.dominio.Veiculo;
import br.ucsal.testequalidade20192.locadora.exception.ClienteNaoEncontradoException;
import br.ucsal.testequalidade20192.locadora.persistence.ClienteDAO;
import br.ucsal.testequalidade20192.locadora.persistence.LocacaoDAO;
import br.ucsal.testequalidade20192.locadora.persistence.VeiculoDAO;

/**
 * 
 * Atividade 06 - Powermock 
 * Testes e Qualidade de Software - ADS 2020.1
 * 
 * @author Luan Souza e Renan Torrentes
 *
 */

@RunWith(PowerMockRunner.class)
@PrepareForTest({ LocacaoBO.class, ClienteDAO.class, VeiculoDAO.class, LocacaoDAO.class })
public class LocacaoBOUnitarioTest {

	private static Cliente cliente;
	private static Veiculo veiculo;
	private static List<Veiculo> listVeiculos;
	private static Locacao locacao;
	private static List<String> listPlacas;

	@BeforeClass
	public static void setupMock() {
		cliente = new Cliente("12345678910", "Jo�o Silva", "71991254789");
		veiculo = new Veiculo("BRA2O20", 2019, new Modelo("Hyundai HB20S"), 62.65);
		listVeiculos = new ArrayList<Veiculo>();
		listVeiculos.add(veiculo);
		locacao = new Locacao(cliente, listVeiculos, new Date(), 5);
		listPlacas = new ArrayList<String>();
		listPlacas.add(veiculo.getPlaca());
	}

	/**
	 * Verificar se ao locar um veiculo disponivel para um cliente cadastrado, um
	 * contrato de locacao eh inserido.
	 * 
	 * Metodo:
	 * 
	 * public static Integer locarVeiculos(String cpfCliente, List<String> placas,
	 * Date dataLocacao, Integer quantidadeDiasLocacao) throws
	 * ClienteNaoEncontradoException, VeiculoNaoEncontradoException,
	 * VeiculoNaoDisponivelException, CampoObrigatorioNaoInformado
	 *
	 * Observacao1: lembre-se de mocar os metodos necessarios nas classes
	 * ClienteDAO, VeiculoDAO e LocacaoDAO.
	 * 
	 * Observacao2: lembre-se de que o metodo locarVeiculos eh um metodo command.
	 * 
	 * @throws ClienteNaoEncontradoException
	 * 
	 * @throws Exception
	 */

	@Test
	public void locarClienteCadastradoUmVeiculoDisponivel() throws Exception {

		// Mock ClienteDAO
		mockStatic(ClienteDAO.class);
		when(ClienteDAO.obterPorCpf("12345678910")).thenReturn(cliente);

		// Mock VeiculoDAO
		mockStatic(VeiculoDAO.class);
		when(VeiculoDAO.obterPorPlaca("BRA2O20")).thenReturn(veiculo);

		// Mock LocacaoDAO
		mockStatic(LocacaoDAO.class);

		// Quando uma inst�ncia de Locacao � criada
		whenNew(Locacao.class).withAnyArguments().thenReturn(locacao);

		// Locar veiculo
		LocacaoBO.locarVeiculos("12345678910", listPlacas, new Date(), 5);

		// Verificar
		verifyStatic(ClienteDAO.class);
		ClienteDAO.obterPorCpf("12345678910");

		verifyStatic(VeiculoDAO.class);
		VeiculoDAO.obterPorPlaca("BRA2O20");

		verifyStatic(LocacaoDAO.class);
		LocacaoDAO.insert(locacao);

		verifyNoMoreInteractions(LocacaoBO.class);
	}
}
