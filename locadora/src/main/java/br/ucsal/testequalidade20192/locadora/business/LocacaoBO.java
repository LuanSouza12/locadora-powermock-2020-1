package br.ucsal.testequalidade20192.locadora.business;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import br.ucsal.testequalidade20192.locadora.dominio.Cliente;
import br.ucsal.testequalidade20192.locadora.dominio.Locacao;
import br.ucsal.testequalidade20192.locadora.dominio.Veiculo;
import br.ucsal.testequalidade20192.locadora.dominio.enums.SituacaoVeiculoEnum;
import br.ucsal.testequalidade20192.locadora.exception.CampoObrigatorioNaoInformado;
import br.ucsal.testequalidade20192.locadora.exception.ClienteNaoEncontradoException;
import br.ucsal.testequalidade20192.locadora.exception.VeiculoNaoDisponivelException;
import br.ucsal.testequalidade20192.locadora.exception.VeiculoNaoEncontradoException;
import br.ucsal.testequalidade20192.locadora.persistence.ClienteDAO;
import br.ucsal.testequalidade20192.locadora.persistence.LocacaoDAO;
import br.ucsal.testequalidade20192.locadora.persistence.VeiculoDAO;

public class LocacaoBO {

	/**
	 * Verifica se as condicoes de locacao sao atendidas:
	 * 
	 * 1. Cliente deve estar previamente cadastrado;
	 * 
	 * 2. O(s) veiculo(s) selecionado(s) deve(m) estar cadastrados;
	 * 
	 * 3. O(s) veiculo(s) selecionado(s) deve(m) estar disponiveis;
	 * 
	 * 4. Devem ser informados: data de locacao e quantidade dias de locacao.
	 * 
	 * Caso as condicoes tenham sido atendidas, este metodo realizara o
	 * cadastramento do contrato de locacao na base.
	 * 
	 * O nao atendimento das condicoes levantara uma excessao.
	 * 
	 * @param cpfCliente            - CPF do cliente que esta contratando a locacao
	 *                              do veiculo(s)
	 * @param placas                - placa(s) do(s) veiculo(s) que serao locados
	 * @param dataLocacao           - data de inicio da locacao
	 * @param quantidadeDiasLocacao - quantidade de dias de locacao
	 * @return
	 * @throws ClienteNaoEncontradoException - cliente nao cadastrado
	 * @throws VeiculoNaoEncontradoException - pelo menos um dos veiculos
	 *                                       selecionados para locacao nao esta
	 *                                       cadastrado
	 * @throws VeiculoNaoDisponivelException - pelo menos um dos veiculos
	 *                                       selecionados para locacao nao esta
	 *                                       disponivel
	 * @throws CampoObrigatorioNaoInformado  - pelo menos um dos campo obrigatório
	 *                                       não foi informado
	 */

	public static void locarVeiculos(String cpfCliente, List<String> placas, Date dataLocacao,
			Integer quantidadeDiasLocacao) throws ClienteNaoEncontradoException, VeiculoNaoEncontradoException,
			VeiculoNaoDisponivelException, CampoObrigatorioNaoInformado {

		Cliente cliente = ClienteDAO.obterPorCpf(cpfCliente);

		List<Veiculo> veiculos = new ArrayList<>();
		for (String placa : placas) {
			Veiculo veiculo = VeiculoDAO.obterPorPlaca(placa);
			if (!SituacaoVeiculoEnum.DISPONIVEL.equals(veiculo.getSituacao())) {
				throw new VeiculoNaoDisponivelException();
			}
			veiculos.add(veiculo);
		}

		Locacao locacao = new Locacao(cliente, veiculos, dataLocacao, quantidadeDiasLocacao);

		LocacaoDAO.insert(locacao);
	}

}
